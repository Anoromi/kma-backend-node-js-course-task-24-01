"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getWeekDay = exports.dayCountBetween = exports.dayCountForYear = exports.isLeapYear = void 0;
function isLeapYear(day) {
    if (day.getFullYear() % 400 === 0)
        return true;
    if (day.getFullYear() % 100 === 0)
        return false;
    return day.getFullYear() % 4 === 0;
}
exports.isLeapYear = isLeapYear;
function dayCountForYear(date) {
    if (isLeapYear(date))
        return 366;
    return 365;
}
exports.dayCountForYear = dayCountForYear;
function dayCountBetween(start, end) {
    let startReal;
    let endReal;
    if (start.getTime() < end.getTime()) {
        startReal = new Date(start);
        endReal = new Date(end);
    }
    else {
        startReal = new Date(end);
        endReal = new Date(start);
    }
    let count = 0;
    if (startReal.getFullYear() === endReal.getFullYear())
        return getYearDay(endReal) - getYearDay(startReal);
    if (isLeapYear(startReal) && start.getMonth() == 1 && start.getDate() === 28) {
        start.setDate(start.getDate() - 1);
    }
    count += dayCountForYear(startReal) - getYearDay(startReal);
    startReal.setFullYear(startReal.getFullYear() + 1);
    while (startReal.getFullYear() < endReal.getFullYear()) {
        count += dayCountForYear(startReal);
        startReal.setFullYear(startReal.getFullYear() + 1);
    }
    count += getYearDay(endReal);
    return count;
}
exports.dayCountBetween = dayCountBetween;
function getYearDay(date) {
    let count = 0;
    for (let month = 0; month < date.getMonth(); month++) {
        count += daysInMonth(month, date);
    }
    count += date.getDate();
    return count;
}
function daysInMonth(month, date) {
    if (month == 1 && isLeapYear(date))
        return 29;
    if (month == 1)
        return 28;
    if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11)
        return 31;
    return 30;
}
function getWeekDay(date) {
    switch (date.getDay()) {
        case 0:
            return 'Sunday';
        case 1:
            return 'Monday';
        case 2:
            return 'Tuesday';
        case 3:
            return 'Wednesday';
        case 4:
            return 'Thursday';
        case 5:
            return 'Friday';
        case 6:
            return 'Saturday';
    }
}
exports.getWeekDay = getWeekDay;
//# sourceMappingURL=dateUtils.js.map