export declare function isLeapYear(day: Date): boolean;
export declare function dayCountForYear(date: Date): number;
export declare function dayCountBetween(start: Date, end: Date): number;
export declare function getWeekDay(date: Date): "Sunday" | "Monday" | "Tuesday" | "Wednesday" | "Thursday" | "Friday" | "Saturday";
