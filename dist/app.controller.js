"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const app_service_1 = require("./app.service");
const rawbody = require("raw-body");
const dateUtils_1 = require("./utils/dateUtils");
let AppController = class AppController {
    constructor(appService) {
        this.appService = appService;
    }
    getHello() {
        return this.appService.getHello();
    }
    async square(req) {
        const text = await (await rawbody(req)).toString('utf8');
        let value = parseFloat(text);
        if (isNaN(value))
            value = 0;
        return { number: value, square: value * value };
    }
    async reverse(req) {
        const text = await (await rawbody(req)).toString('utf8');
        let result = '';
        for (let i = text.length - 1; i >= 0; i--) {
            result += text[i];
        }
        return result;
    }
    async date(req, year, month, day) {
        const yearValue = parseInt(year);
        const monthValue = parseInt(month);
        const dayValue = parseInt(day);
        if (isNaN(yearValue) || isNaN(monthValue) || isNaN(dayValue))
            throw new common_1.HttpException("Illegal values", common_1.HttpStatus.BAD_REQUEST);
        const date = new Date(yearValue, monthValue - 1, dayValue);
        return {
            difference: (0, dateUtils_1.dayCountBetween)(date, new Date()),
            isLeapYear: (0, dateUtils_1.isLeapYear)(date),
            weekDay: (0, dateUtils_1.getWeekDay)(date)
        };
    }
};
exports.AppController = AppController;
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], AppController.prototype, "getHello", null);
__decorate([
    (0, common_1.Post)("/square"),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "square", null);
__decorate([
    (0, common_1.Post)("/reverse"),
    (0, common_1.Header)("content-type", "text/plain"),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "reverse", null);
__decorate([
    (0, common_1.Get)("/date/:year/:month/:day"),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)("year")),
    __param(2, (0, common_1.Param)("month")),
    __param(3, (0, common_1.Param)("day")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, String, String]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "date", null);
exports.AppController = AppController = __decorate([
    (0, common_1.Controller)(),
    __metadata("design:paramtypes", [app_service_1.AppService])
], AppController);
//# sourceMappingURL=app.controller.js.map