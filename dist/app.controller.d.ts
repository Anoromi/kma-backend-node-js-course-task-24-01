import { AppService } from "./app.service";
import { Request } from "express";
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getHello(): string;
    square(req: Request): Promise<{
        number: number;
        square: number;
    }>;
    reverse(req: Request): Promise<string>;
    date(req: Request, year: string, month: string, day: string): Promise<{
        difference: number;
        isLeapYear: boolean;
        weekDay: string;
    }>;
}
