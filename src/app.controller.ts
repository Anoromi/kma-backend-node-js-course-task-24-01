import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  UsePipes,
  ValidationPipe,
  Header,
  Param,
  HttpException,
  HttpStatus
} from "@nestjs/common";
import { AppService } from "./app.service";
import { Request } from "express";
import * as rawbody from 'raw-body';
import { dayCountBetween, getWeekDay, isLeapYear } from "./utils/dateUtils";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post("/square")
  async square(@Req() req: Request): Promise<{ number: number; square: number; }> {
    const text = await (await rawbody(req)).toString('utf8')
    let value = parseFloat(text)
    if(isNaN(value))
      value = 0
    return { number: value, square: value * value };
  }

  @Post("/reverse")
  @Header("content-type", "text/plain")
  async reverse(@Req() req: Request): Promise<string> {
    const text = await (await rawbody(req)).toString('utf8')
    let result = ''
    for (let i = text.length - 1; i >= 0; i--) {
      result += text[i]
    }
    return result
  }

  @Get("/date/:year/:month/:day")
  async date(@Req() req: Request, @Param("year") year : string, @Param("month") month: string, @Param("day") day: string) {
    const yearValue = parseInt(year)
    const monthValue = parseInt(month)
    const dayValue = parseInt(day)

    if(isNaN(yearValue) || isNaN(monthValue) || isNaN(dayValue))
      throw new HttpException("Illegal values", HttpStatus.BAD_REQUEST)


    const date = new Date(yearValue, monthValue - 1, dayValue)
    return {
      difference: dayCountBetween(date, new Date()),
      isLeapYear: isLeapYear(date),
      weekDay: getWeekDay(date)
    }
  }
}
